//	sætter R0=n og afslutter, hvis n<0.
	@R0
	D=M
	@END
	D;JLT
//	Sætter R1=n og afslutter, hvis n=0 eller n=1.
	@R1
	M=D
	@END
	D;JEQ
	D-1;JEQ
//	sætter R1=1=pop(1) (og R2=0=pop(0)).
	@1
	D=A
	@R1
	M=D
	@0
	D=A
	@R2
	M=D
	
//R4 = int i = n-1 (loopet skal køre n-1 gange)
	@R0
	D=M
	@R4
	M=D-1
(LOOP) //i = n-1; i>0; i--;
	@R4
	D=M
	@END
	D;JEQ
	@R1
	D=M
	@R3
	M=D

//	henter værdierne 4 og 2 ind i hhv. R5=4 og R6=2
//	4 i stedet for 5, fordi man allerede har værdien 1 gang.
	@4
	D=A
	@R5
	M=D
	@2
	D=A
	@R6
	M=D	
	
//	pop(n-1)*5
(LOOP1)
	@R5
	D=M
	@END1
	D;JEQ
	@R3
	D=M
	@R1
	M=M+D
	@R5
	M=M-1
	@LOOP1
	0;JMP
(END1)
	
//	trækker 2 fra R1, så kommandoen JLT kan benyttes i loopet
	@R1
	D=M-1
	D=D-1
	@R7
	M=D
	@R1
	M=0
//	del R1 med 2	
(LOOP2)
	@R7
	D=M
	@END2
	D;JLT
	@R6
	D=M
	@R7
	M=M-D
	@R1
	M=M+1
	@LOOP2
	0;JMP
(END2)
	
//	R1=R1-R2, hvor R2=pop(n-2)
	@R2
	D=M
	@R1
	M=M-D
//	ny pop(n-2) gemmes i R2
	@R3
	D=M
	@R2
	M=D
//	omgangen er overstået
	@R4
	M=M-1
	@LOOP
	0;JMP
(END)
	@END
	0;JMP